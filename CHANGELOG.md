# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- markdownlint-disable MD012 MD024 -->

## Unreleased

- Disk role
- Selinux role

## [0.2.0] - 2024-01-31

### Added

- Initial roles

## [0.1.6] - 2024-01-29

### Fixed

- Wiki links againx5

## [0.1.5] - 2024-01-29

### Fixed

- Wiki links againx4

## [0.1.4] - 2024-01-29

### Fixed

- Wiki links againx3

## [0.1.3] - 2024-01-29

### Fixed

- Wiki links againx2

## [0.1.2] - 2024-01-29

### Fixed

- Wiki links again

## [0.1.1] - 2024-01-29

### Fixed

- Wiki links

## [0.1.0] - 2024-01-28

### Added

- Package role
- Auth role
- Firewall role
- SSH role
- NTP role
