# Contributing

When contributing to this repository, please first discuss the change
you wish to make via issue, email, or any other method with
the owners of this repository before making a change.

Please note we have a [Code of Conduct](CODE_OF_CONDUCT.md),
please follow it in all your interactions with the project.

## Issues

Feel free to file an [issue][url-issue]
if you notice bugs or have a feature request.

## Merge Requests

To contribute features or bugfixesplease submit a [Merge Request][url-merge].

1. Ensure you adhere to any styling or linting rules
    that might be present in this project.
1. Update the [Readme](README.md) and relative files with documentation
    of changes made to the project.
1. Update the [Changelog](CHANGELOG.md) file with details
    and an incremented version that this
    Merge Request would represent.
    The versioning scheme we use is [Semantic Versioning][url-semver].

[url-issue]: https://docs.gitlab.com/ee/user/project/issues/create_issues.html
[url-merge]: https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html
[url-semver]: https://semver.org/spec/v2.0.0.html
