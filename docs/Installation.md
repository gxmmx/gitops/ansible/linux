<!-- markdownlint-disable MD041 -->
**[wiki](home)** /

## Ansible Galaxy installation

The collection is available on Ansible Galaxy.  
Installation can be performed using the `ansible-galaxy` CLI.

**`＞`**

``` shell
ansible-galaxy collection install gxmmx.linux
```

The collection can also be installed using a `requirements.yml` file.

**📄 `requirements.yml`**

``` yaml
collections:
  - name: gxmmx.linux
```

**`＞`**

``` shell
ansible-galaxy collection install -r requirements.yml
```

## Git installation

The collection can also be directly installed by cloning the repository.

**`＞`**

``` shell
dir="~/.ansible/collections/ansible_collections/gxmmx"
mkdir -p "$dir"
git clone "https://gitlab.com/gxmmx/gitops/ansible/linux.git" "${dir}/linux"
```

> ❗ This is one of the default locations Ansible looks for collections,  
>ensure you set the `ANSIBLE_COLLECTIONS_PATH` environment variable
> or the equivalent config variable in `ansible.cfg`.  
> See [Ansible documentation][url-ansible-config] for details.

[url-ansible-config]: https://docs.ansible.com/ansible/latest/reference_appendices/config.html#collections-paths
