<!-- markdownlint-disable MD041 -->
**[wiki](home)** /

## [gxmmx.linux.backup_file](Modules/Backup-File)

Module that handles backup files made by
[ansible.builtin.copy][url-ansible-copy-module] and
[ansible.builtin.template][url-ansible-template-module].

When `backup: true` is defined, Ansible creates backup files with
specific datetime suffixes.  
By providing a path to a file, that has been backed up, this module
can rotate the backups and define a specific amount of files to keep.

It can also restore the lastest backup or a specified version.

**Key features:**

* Manages backed up files created by Ansible.
* Backup rotation.
* Backup restoration.

[url-ansible-copy-module]: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html
[url-ansible-template-module]: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html
