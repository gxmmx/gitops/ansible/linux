<!-- markdownlint-disable MD041 -->
[wiki](home) /
**[Modules](Modules)** /

## Getting started

Simple example of rotating a backup to ensure no more than 5
most recent files are retained.

**📄 `site.yml`**

``` yaml
- name: Simple Auth Play
  hosts: all

  tasks:
    - name: Ensure file
      ansible.builtin.template:
        src: mytemplate.j2
        dest: /path/to/file.conf
        backup: true

    - name: Ensure backup rotation:
      gxmmx.linux.backup_file:
        path: /path/to/file.conf
        keep: 5
```

## Information

### Platforms

Tests in place for listed platforms.
Backwards compatability not guaranteed but likely supported.

* Centos Stream 8
* Centos Stream 9
* Rocky Linux 8
* Rocky Linux 9
* Oracle Linux 8
* Oracle Linux 9
* Debian 11
* Debian 12

### Requirements

* Ansible Core >=2.16

### Dependancies

* None
