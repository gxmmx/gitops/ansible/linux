<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Modules](Modules) /
**[Backup File](Modules/Backup-File)** /

## Parameters

Module parameters for configuration.

``` yaml
gxmmx.linux.backup_file:
  ## Path to file that was backed up
  # Required :  true
  # Type     :  string
  path: '/path/to/file.conf'

  ## Backup to restore
  ## Can either reference 'latest' or the filename
  ## of a specific backup.
  ## When registering a variable from a copy or template task
  ## the registered_var.backup_file holds this name.
  # Required :  false
  # Type     :  string
  # Options  :  latest|filename
  restore: 'latest'

  ## Amount to keep
  # Required :  false
  # Type     :  integer
  keep: 5
```
