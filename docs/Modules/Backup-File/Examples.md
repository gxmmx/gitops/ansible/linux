<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Modules](Modules) /
**[Backup File](Modules/Backup-File)** /

## Rotate

Simple example of rotating a backup to ensure no more than 5
most recent files are retained.

**📄 `site.yml`**

``` yaml
- name: Simple Backup Play
  hosts: all

  tasks:
    - name: Ensure file
      ansible.builtin.template:
        src: mytemplate.j2
        dest: /path/to/file.conf
        backup: true

    - name: Ensure backup rotation:
      gxmmx.linux.backup_file:
        path: /path/to/file.conf
        keep: 5
```

## Restore latest

Example that restores latest backup on condition.

**📄 `site.yml`**

``` yaml
- name: Restore Latest Play
  hosts: all

  tasks:
    - name: Ensure file
      ansible.builtin.template:
        src: mytemplate.j2
        dest: /path/to/file.conf
        backup: true

    - name: Some condition
      ansible.builtin.set_fact:
        emulate_fail: true

    - name: Ensure restore if failure
      gxmmx.linux.backup_file:
        path: /path/to/file.conf
        restore: latest
      when: emulate_fail | bool

```

## Restore Specific

Example that restores a specific backup on condition.  
Also ensures rotation.

**📄 `site.yml`**

``` yaml
- name: Restore Specific Play
  hosts: all

  tasks:
    - name: Ensure file
      ansible.builtin.template:
        src: mytemplate.j2
        dest: /path/to/file.conf
        backup: true
      register: templated_file

    - name: Some condition
      ansible.builtin.set_fact:
        emulate_fail: true

    - name: Ensure restore if failure
      gxmmx.linux.backup_file:
        path: /path/to/file.conf
        restore: "{{ templated_file.backup_file }}"
        keep: 5
      when: emulate_fail | bool

```
