<!-- markdownlint-disable MD041 -->
**[wiki](home)** /

## [gxmmx.linux.auth](Roles/Auth)

Role for user and group provisioning.

Handles authentication and authorization.  
It is essentially wrapper for the
[ansible.builtin.user][url-ansible-user-module]
and [ansible.builtin.group][url-ansible-group-module] modules,
that handles idempotent creation and deletion of users, groups
and their data.

**Key features:**

* Provisions users and groups
* Ensures authorized keys
* Ensures sudoers permissions

## [gxmmx.linux.firewall](Roles/Firewall)

Role for provisioning firewall.

Sets up and configures [NFTables][url-nftables].  
Rules are validated and parsed, state is ensured.  
Can create a rollback service to ensure connection after configuration
changes, that reverts if connection is lost.  

**Key features:**

* Configures and sets up firewall.
* Parses and validates rules.
* Ensures rollback if reconnect fails.
* Supports raw NFTables rules if needed.

## [gxmmx.linux.ntp](Roles/NTP)

Role for provisioning Network Time Protocol.

Sets up and configures [Chrony][url-chrony].
Can configure host both as server and client.

**Key features:**

* Configures Network Time Protocol.

## [gxmmx.linux.package](Roles/Package)

Role for installing and uninstalling packages.

Uses the distributions native package manager but is built
with checks for idempotency to ensure performance and reusability.  
The role is included in multiple other roles in the collection.

**Key features:**

* Installs packages
* Uninstalls packages

## [gxmmx.linux.ssh](Roles/SSH)

Role for configuring and hardening SSH.

Sets custom configuratation and validates SSH daemon settings.  
Can be used to set custom hostkeys or rotate the current ones.

**Key features:**

* Configures SSH server.
* Configures SSH host keys.

[url-ansible-user-module]: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html
[url-ansible-group-module]: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/group_module.html
[url-nftables]: https://wiki.nftables.org
[url-chrony]: https://chrony-project.org
