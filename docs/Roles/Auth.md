<!-- markdownlint-disable MD041 -->
[wiki](home) /
**[Roles](Roles)** /

## Getting started

Simple example of ensuring a user with an authorized key.

**📄 `site.yml`**

``` yaml
- name: Simple Auth Play
  hosts: all

  vars:
    auth_user_list:
      - name: my_user
        state: present
        pubkeys:
          - 'ssh-ed25519 AW...Q05Z3l9sJ my_user@host'

  roles:
    - gxmmx.linux.auth
```

## Information

### Platforms

Tests in place for listed platforms.
Backwards compatability not guaranteed but likely supported.

* Centos Stream 8
* Centos Stream 9
* Rocky Linux 8
* Rocky Linux 9
* Oracle Linux 8
* Oracle Linux 9
* Debian 11
* Debian 12

### Requirements

* Ansible Core >=2.16

### Dependancies

* None
