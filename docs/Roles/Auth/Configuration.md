<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[Auth](Roles/Auth)** /

## Tags

Ansible tags available for role.

| Tag            | Description                  |
| -------------- | ---------------------------- |
| `auth`         | Global tag for running role. |

## Variables

### Settings

Settings that affect role behaviour and actions.

``` yaml
## Enable role execution
# Default  :  true
# Type     :  bool
auth_role_enabled: true
```

``` yaml
## Permit management of lists defined
# Default  :  true
# Type     :  bool
auth_settings_manage_list: true
```

``` yaml
## Permit management of root user
## Added as a precaution for automated runs that update all users.
# Default  :  false
# Type     :  bool
auth_settings_manage_root: false
```

``` yaml
## Permit management of provision user
## Added as a precaution for automated runs that update all users.
## Skip og include the user that Ansible connected with.
# Default  :  false
# Type     :  bool
auth_settings_manage_provision: false
```

### Data

Data used by role that determines content.

``` yaml
## Root users password hash
## Has to be a valid hash, cleartext passwords not accepted.
# Default  :  ''
# Type     :  string
# Required :  When auth_settings_manage_root: true
auth_root_password: ''
```

``` yaml
## Root users public SSH keys
## Keys ensured in authorized_keys.
# Default  :  []
# Type     :  list(strings)
auth_root_pubkeys: []
```

``` yaml
## List of users to manage
## Will also parse all variables like:
## auth_user_list_*
## for segmentation purposes. 
# Default  :  []
# Type     :  list(dict)
auth_user_list: []

# Keys:
auth_user_list:
    ## Users name
    ## Will also ensure named primary group
    # Required :  true
    # Type     :  string
  - name: 'username'

    ## Users state
    # Default  :  present
    # Type     :  string
    # Options  :  present|absent|disabled
    state: 'present'

    ## Users expiry
    ## Accepts the string never, as well as formated date.
    # Default  :  never
    # Type     :  string
    # Options  :  never|YYYY-MM-DD
    expires: 'never'

    ## Users UID
    # Default  :  next available id
    # Type     :  integer
    id: 9999

    ## Users GECOS comment
    # Default  :  ''
    # Type     :  string
    comment: ''

    ## Users login shell
    ## Accepts 'default', 'nologin' or shell name.
    ## Will ensure installable shells.
    ## See auth_vars_shells_native and auth_vars_shells_installable for options.
    # Default  :  default
    # Type     :  string
    # Options  :  default|nologin|shellname
    shell: 'default'

    ## Users home directory
    ## Accepts 'default' or path to home dir.
    ## Will try to move home as per ansible.builtin.user module.
    # Default  :  default
    # Type     :  string
    # Options  :  default|path/to/home
    home: 'default'

    ## Users groups
    ## Groups must exist. Ensure with auth_group_list.
    # Default  :  []
    # Type     :  list(strings)
    groups: []

    ## Users sudoers permissions
    ## Example: ['ALL=(ALL:ALL) ALL:ALL']
    ## Example: ['ALL=(ALL:ALL) NOPASSWD:ALL']
    # Default  :  []
    # Type     :  list(strings)
    sudo: []

    ## Users password hash
    ## Has to be a valid hash, cleartext passwords not accepted.
    ## See auth_vars_hash_prefixes for options.
    ## Empty string for disabled password.
    # Default  :  ''
    # Type     :  string
    password: ''

    ## Users public SSH keys
    ## Populates authorized_keys for user.
    ## Example: ['ssh-ed25519 ...']
    # Default  :  []
    # Type     :  list(strings)
    pubkeys: []
```

``` yaml
# List of groups to manage
# Will also parse all variables like:
# auth_group_list_*
# for segmentation purposes. 

# Default  :  []
# Type     :  list(strings)
# Required :  false

auth_group_list: []

# Keys:
auth_group_list:
    ## Groups name
    ## Should not list users primary groups.
    # Required :  true
    # Type     :  string
  - name: 'groupname'

    ## Groups state
    # Default  :  present
    # Type     :  string
    # Options  :  present|absent
    state: 'present'

    ## Groups GID
    # Default  :  next available id
    # Type     :  integer
    id: 7777

    ## Groups sudoers permissions
    ## Example: ['ALL=(ALL:ALL) ALL:ALL']
    ## Example: ['ALL=(ALL:ALL) NOPASSWD:ALL']
    # Default  :  []
    # Type     :  list(strings)
    sudo: []
```
