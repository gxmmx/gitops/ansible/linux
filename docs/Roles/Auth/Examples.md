<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[Auth](Roles/Auth)** /

## Simple

Simple example of ensuring a user with an authorized key.

**📄 `site.yml`**

``` yaml
- name: Ensure My User
  hosts: all

  vars:
    auth_user_list:
      - name: my_user
        state: present
        pubkeys:
          - 'ssh-ed25519 AAAAC3N...Z3l9/sJ my_user@host'

  roles:
    - gxmmx.linux.auth
```

## Complete

Complete example, utilizing most all role variables.  
Showcases using named lists for organization
as well as setting custom parameters.  
Also showcases removal and disabling users.

**📄 `site.yml`**

``` yaml
- name: Configure Users
  hosts: all

  vars:
    auth_settings_manage_list: true
    auth_settings_manage_root: true

    auth_root_password: "$6$W.mXB.58oxDf...dmK7k/"
    auth_root_pubkeys: ['ssh-ed25519 AAA...h8YRi']

    auth_user_list:
      - name: some_user
        state: present
        expires: "2030-01-20"
        id: 2001
        comment: 'user_comment'
        shell: zsh
        home: /some_user
        groups: ['group1','group2']
        sudo: ['ALL=(ALL:ALL) NOPASSWD:/bin/true']
        password: '$6$Gz157n8dx...0b30zOXgy6wAN/96wrU1'
        pubkeys:
          - 'ssh-ed25519 AAADI1...5Z3l9/sJ some_user@host1'
          - 'ssh-ed25519 AAAAC3...ySx7+uhV some_user@host2'

    auth_user_list_named:
      - name: disabled_user
        state: disabled
        shell: default
      - name: user_with_no_login
        state: present
        shell: nologin
      - name: user_to_remove
        state: absent

    auth_group_list:
      - name: group1
        id: 4001
        sudo: ['ALL=(ALL:ALL) NOPASSWD:/bin/true']

    auth_group_list_named:
      - name: group2

  roles:
    - gxmmx.linux.auth
```
