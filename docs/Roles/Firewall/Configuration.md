<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[Firewall](Roles/Firewall)** /

## Tags

Ansible tags available for role.

| Tag            | Description                  |
| -------------- | ---------------------------- |
| `firewall`     | Global tag for running role. |

## Variables

### Settings

Settings that affect role behaviour and actions.

``` yaml
## Enable role execution
# Default  :  true
# Type     :  bool
firewall_role_enabled: true
```

``` yaml
## Purge config
## Purges configuration to ensure no rules have been added manually.
## This is not an idempotent action when true.
# Default  :  false
# Type     :  bool
firewall_settings_purge_config: false
```

``` yaml
## Rollback on failure
## Creates a rollback service and rolls back to previous backup
## if connection fails after reconfiguration.
# Default  :  false
# Type     :  bool
firewall_settings_rollback_on_connection_failure: false
```

``` yaml
## Rollback timeout
## Timeout to wait for reconnect after ruleset reload.
# Default  :  60
# Type     :  integer
firewall_settings_rollback_timeout: 60
```

``` yaml
## Rollback backups
## How many backups to keep for rollback.
# Default  :  5
# Type     :  integer
firewall_settings_rollback_backups: 5
```

### Config

Configuration of role parameters.

``` yaml
## Inbound policy
# Default  :  drop
# Type     :  string
# Options  :  drop|accept
firewall_config_inbound_policy: drop
```

``` yaml
## Outbound policy
# Default  :  accept
# Type     :  string
# Options  :  drop|accept
firewall_config_outbound_policy: accept
```

``` yaml
## Allow established and related
# Default  :  true
# Type     :  bool
firewall_config_inbound_allow_established_and_related: true
```

``` yaml
## Allow invalid
# Default  :  false
# Type     :  bool
firewall_config_inbound_allow_invalid: false
```

``` yaml
## Allow loopback
# Default  :  true
# Type     :  bool
firewall_config_inbound_allow_loopback: true
```

``` yaml
## Allow ping
# Default  :  true
# Type     :  bool
firewall_config_inbound_allow_ping: true
```

``` yaml
## Inbound rules
# Default  :  allow ssh rule below
# Type     :  list(dict)
firewall_config_inbound_rules:
  - name: "Allow SSH"
    comment: "Allow SSH"
    port: ssh
    protocol: tcp
    policy: accept

# Keys:
firewall_config_inbound_rules:
    ## Rule name
    # Required :  true
    # Type     :  string
  - name: 'rule name'

    ## Rule comment
    # Default  : rule name value
    # Type     :  string
    comment: 'rule comment'

    ## Policy
    # Default  :  drop
    # Type     :  string
    # Options  :  drop|accept
    policy: 'accept'

    ## Interface name
    # Required : one of (interface|port|source|destination)
    # Type     :  string|list(strings)
    interface: "en0"

    ## Port number
    ## Can also be service name defined in /etc/services
    # Required : one of (interface|port|source|destination)
    # Type     :  string|list(strings)
    port: "22"

    ## Source IP
    ## Can be IP, CIDR or IP range.
    # Required : one of (interface|port|source|destination)
    # Type     :  string|list(strings)
    source: "10.10.10.10"

    ## Destination IP
    ## Can be IP, CIDR or IP range.
    # Required : one of (interface|port|source|destination)
    # Type     :  string|list(strings)
    destination: "10.10.10.10"
```

``` yaml
## Outbound rules
# Default  :  []
# Type     :  list(dict)
firewall_config_outbound_rules: []

# Keys:
firewall_config_outbound_rules:
    ## Rule name
    # Required :  true
    # Type     :  string
  - name: 'rule name'

    ## Rule comment
    # Default  : rule name value
    # Type     :  string
    comment: 'rule comment'

    ## Policy
    # Default  :  drop
    # Type     :  string
    # Options  :  drop|accept
    policy: 'accept'

    ## Interface name
    # Required : one of (interface|port|source|destination)
    # Type     :  string|list(strings)
    interface: "en0"

    ## Port number
    ## Can also be service name defined in /etc/services
    # Required : one of (interface|port|source|destination)
    # Type     :  string|list(strings)
    port: "22"

    ## Source IP
    ## Can be IP, CIDR or IP range.
    # Required : one of (interface|port|source|destination)
    # Type     :  string|list(strings)
    source: "10.10.10.10"

    ## Destination IP
    ## Can be IP, CIDR or IP range.
    # Required : one of (interface|port|source|destination)
    # Type     :  string|list(strings)
    destination: "10.10.10.10"
```

``` yaml
## Inbound raw rules
# Default  :  []
# Type     :  list(strings)
firewall_config_inbound_raw: []
```

``` yaml
## Outbound raw rules
# Default  :  []
# Type     :  list(strings)
firewall_config_outbound_raw: []
```
