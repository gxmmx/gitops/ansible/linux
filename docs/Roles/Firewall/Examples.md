<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[Firewall](Roles/Firewall)** /

## Simple

Simple example that ensures firewall is active.  
Enables SSH access from everywhere by default.

**📄 `site.yml`**

``` yaml
- name: Simple Firewall
  hosts: all

  roles:
    - gxmmx.linux.firewall
```

## Extended

Example that sets up firewall with custom inbound and outbound rules.  
Rollback service is enabled to ensure that connection
is viable after configuration change.

**📄 `site.yml`**

``` yaml
- name: Extended Firewall
  hosts: all

  vars:
    firewall_settings_rollback_on_connection_failure: true
    firewall_settings_rollback_timeout: 60
    firewall_settings_rollback_backups: 5
    firewall_config_inbound_rules:
      - name: "Allow SSH"
        comment: "Allow SSH"
        port: ssh
        protocol: tcp
        policy: accept
      - name: "My Inbound Service"
        comment: "Example inbound service"
        source:
          - "10.10.10.0/24"
          - "10.10.11.102-10.10.11.200"
        port: [443]
        protocol: any
        policy: accept
    firewall_config_outbound_rules:
      - name: "My Outbound Service"
        comment: "Example outbound service" 
        destination: "10.10.10.11"
        port: [80,443,59]
  roles:
    - gxmmx.linux.firewall
```

## Raw

Example that shows raw rules being added to firewall configuration.

**📄 `site.yml`**

``` yaml
- name: Raw Firewall
  hosts: all

  vars:
    firewall_config_inbound_raw:
      - 'tcp dport ssh accept'
      - 'tcp dport 443 ip saddr { 0.10.11.102-10.10.11.200 } accept'
    firewall_config_outbound_raw:
      - 'udp sport { 80, 443, 59 } ip daddr 10.10.10.11 accept'
  roles:
    - gxmmx.linux.firewall
```
