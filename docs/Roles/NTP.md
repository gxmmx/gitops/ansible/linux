<!-- markdownlint-disable MD041 -->
[wiki](home) /
**[Roles](Roles)** /

## Getting started

Simple example that ensures NTP is running with sane defaults.

**📄 `site.yml`**

``` yaml
- name: Simple NTP Play
  hosts: all

  roles:
    - gxmmx.linux.ntp
```

## Information

### Platforms

Tests in place for listed platforms.
Backwards compatability not guaranteed but likely supported.

* Centos Stream 8
* Centos Stream 9
* Rocky Linux 8
* Rocky Linux 9
* Oracle Linux 8
* Oracle Linux 9
* Debian 11
* Debian 12

### Requirements

* Ansible Core >=2.16

### Dependancies

* None
