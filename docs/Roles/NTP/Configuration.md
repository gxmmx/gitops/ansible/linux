<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[NTP](Roles/NTP)** /

## Tags

Ansible tags available for role.

| Tag            | Description                  |
| -------------- | ---------------------------- |
| `ntp`          | Global tag for running role. |

## Variables

### Settings

Settings that affect role behaviour and actions.

``` yaml
## Enable role execution
# Default  :  true
# Type     :  bool
ntp_role_enabled: true
```

``` yaml
## Validate sources
## Checks if sources are active.
## See ntp_vars_validate_command for details.
# Default  :  false
# Type     :  bool
ntp_settings_validate_source: false
```

### Config

Configuration of role parameters.

> For details on configuration options,
> refer to [Chrony configuration documentation][url-chrony-conf]

``` yaml
## List of NTP sources
## Can list servers and pools.
## Each source can include options.
# Default  :  []
# Type     :  list(dict)
ntp_config_source: []

# Keys:
ntp_config_source:
  ## List of pools
  ## Each pool must have a source, can have options.
  # Default  :  depicted below
  # Type     :  list(dict)
  pools:
    - source: "pool.ntp.org"
      options:
        - iburst
        - prefer

  ## List of servers
  ## Each server must have a source, can have options.
  ## Same structure as for pools.
  # Default  :  []
  # Type     :  list(dict)
  servers: []
```

``` yaml
## List for NTP server
## Can list peers and allow.
## Each source can include options.
# Default  :  []
# Type     :  list(dict)
ntp_config_server: []

# Keys:
ntp_config_server:
  ## List of peers
  ## Each peer must have a source, can have options.
  ## Same structure as for pools.
  # Default  :  depicted below
  # Type     :  list(dict)
  peers: []

  ## List of allows
  ## Each allow must have a source, can have options.
  ## Same structure as for pools.
  ## Allows can be CIDR blocks.
  # Default  :  []
  # Type     :  list(dict)
  allows: []
```

``` yaml
## Configuration options
## Each line is added to configuration.
# Default  :  ["rtcsync","makestep 1.0 3"]
# Type     :  list(strings)
ntp_config_options:
  - "rtcsync"
  - "makestep 1.0 3"
```

``` yaml
## Keys
## Each line is added to key file.
# Default  :  []
# Type     :  list(strings)
ntp_config_keys: []
```

[url-chrony-conf]: https://chrony-project.org/doc/4.4/chrony.conf.html
