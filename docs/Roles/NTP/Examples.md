<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[NTP](Roles/NTP)** /

## Simple

Simple example that ensures NTP is running with sane defaults.

**📄 `site.yml`**

``` yaml
- name: Simple NTP Setup
  hosts: all

  roles:
    - gxmmx.linux.ntp
```

## Client

Example that sets up NTP as a client with custom configuration.  
Validation is performed to ensure that sources are live.

**📄 `site.yml`**

``` yaml
- name: NTP Client Configuration
  hosts: all

  vars:
    ntp_settings_validate_source: true
    ntp_config_source:
      servers:
        - source: "time.google.com"
          options:
            - iburst
            - prefer
      pools:
        - source: "pool.ntp.org"
          options:
            - iburst
    ntp_config_options:
      - "rtcsync"
      - "makestep 1.0 3"

  roles:
    - gxmmx.linux.ntp
```

## Server

Example, that sets up NTP as a server with custom configuration.  
Validation is performed to ensure that sources are live.
The role can config NTP to be source only, but that will need some
hardware clock configuration.

**📄 `site.yml`**

``` yaml
- name: NTP Server Configuration
  hosts: all

  vars:
    ntp_settings_validate_source: true
    ntp_config_server:
      peers:
        - source: '10.1.0.11'
          options:
            - iburst
        - source: '10.1.0.12'
          options:
            - iburst
      allows:
        - source: '192.168.1.0/24'
          options:
            - iburst
        - source: '10.0.0.0/8'
          options:
            - iburst
    ntp_config_source:
      servers:
        - source: "127.127.1.0"
          options:
            - iburst
            - prefer
    ntp_config_options:
      - "local stratum 10"
      - "makestep 0.5 -1"

  roles:
    - gxmmx.linux.ntp
```
