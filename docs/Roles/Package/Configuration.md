<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[Package](Roles/Package)** /

## Tags

Ansible tags available for role.

| Tag            | Description                  |
| -------------- | ---------------------------- |
| `package`      | Global tag for running role. |

## Variables

### Settings

Settings that affect role behaviour and actions.

``` yaml
## Enable role execution
# Default  :  true
# Type     :  bool
package_role_enabled: true
```

``` yaml
## Retries before failure
## Useful if repositories temporarily lose connection.
# Default  :  5
# Type     :  integer
package_settings_retries: 5
```

``` yaml
## Cache seconds for Apt package manager
## Do not refetch cache if within this limit
## Only used for distributions that use apt.
# Default  :  3600
# Type     :  integer
package_settings_apt_cache: 3600
```

### Data

Data used by role that determines content.

``` yaml
## List of packages to install
## A file that the package installs is provided to
## check if install is needed.
# Default  :  []
# Type     :  list(dict)
package_install: []

# Keys:
package_install:
    ## Package name
    # Required :  true
    # Type     :  string
  - package: 'packagename'

    ## Package path
    ## Path to any file installed by the package.
    ## If the package installs a binary, use that.
    # Required :  true
    # Type     :  string
    path: 'path/to/bin/or/file'
```

``` yaml
## List of packages to uninstall
## A file that the package installs is provided to
## check if uninstall is needed.
# Default  :  []
# Type     :  list(dict)
package_uninstall: []

# Keys:
package_uninstall:
    ## Package name
    # Required :  true
    # Type     :  string
  - package: 'packagename'

    ## Package path
    ## Path to any file installed by the package.
    ## If the package installs a binary, use that.
    # Required :  true
    # Type     :  string
    path: 'path/to/bin/or/file'
```

## Outputs

These variables contain valuable data that can be useful in subsequent tasks.

``` yaml
## Install results
package_output_install_result
# Example: Perform task when something was installed
when: package_output_install_result.changed | default(false, true)
```

``` yaml
## Uninstall results
package_output_uninstall_result
# Example: Perform task when something was uninstalled
when: package_output_uninstall_result.changed | default(false, true)
```
