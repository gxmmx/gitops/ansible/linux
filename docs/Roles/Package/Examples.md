<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[Package](Roles/Package)** /

## Simple

Simple example of ensuring a package is installed.

**📄 `site.yml`**

``` yaml
- name: Install Single Package
  hosts: all

  vars:
    package_install:
      - package: nginx
        path: /usr/sbin/nginx

  roles:
    - gxmmx.linux.package
```

## Full

Full example that both uninstalls and installs packages.  
[Nginx][url-nginx] is installed for this web server example.  
Chrony[url-chrony] is installed and [ntpd][url-ntpd],
which conflicts with chrony, is uninstalled.

**📄 `site.yml`**

``` yaml
- name: Install Packages
  hosts: all

  vars:
    package_install:
      - package: nginx
        path: /usr/sbin/nginx
      - package: chrony
        path: /usr/bin/chronyc
    package_uninstall:
      - package: ntp
        path: /usr/bin/ntpd

  roles:
    - gxmmx.linux.package
```

[url-nginx]: https://www.nginx.com
[url-chrony]: https://chrony-project.org
[url-ntpd]: https://www.ntp.org
