<!-- markdownlint-disable MD041 -->
[wiki](home) /
**[Roles](Roles)** /

## Getting started

Simple example that hardens.  
No password login and no root login.

**📄 `site.yml`**

``` yaml
- name: Simple SSH Play
  hosts: all

  vars:
    ssh_settings_validate_connection: false
    ssh_config_permitrootlogin: false
    ssh_config_passwordauthentication: false

  roles:
    - gxmmx.linux.ssh
```

## Information

### Platforms

Tests in place for listed platforms.
Backwards compatability not guaranteed but likely supported.

* Centos Stream 8
* Centos Stream 9
* Rocky Linux 8
* Rocky Linux 9
* Oracle Linux 8
* Oracle Linux 9
* Debian 11
* Debian 12

### Requirements

* Ansible Core >=2.16

### Dependancies

* None
