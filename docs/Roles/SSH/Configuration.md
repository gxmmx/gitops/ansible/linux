<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[SSH](Roles/SSH)** /

## Tags

Ansible tags available for role.

| Tag            | Description                  |
| -------------- | ---------------------------- |
| `ssh`          | Global tag for running role. |

## Variables

### Settings

Settings that affect role behaviour and actions.

``` yaml
## Enable role execution
# Default  :  true
# Type     :  bool
ssh_role_enabled: true
```

``` yaml
## Purge Hostkeys
## Ensures host key rotation.
## This action is not idempotent when true.
# Default  :  false
# Type     :  bool
ssh_settings_purge_hostkeys: false
```

``` yaml
## Validates connection to SSH port
## Only applicable if listenaddress is 0.0.0.0 or 127.0.0.1
# Type     :  bool
# Default  :  true
ssh_settings_validate_connection: true
```

### Config

Configuration of role parameters.

> For details on configuration options,
> refer to [SSH configuration documentation][url-sshd-conf]

[url-sshd-conf]: https://linux.die.net/man/5/sshd_config

#### Connection

``` yaml
## List of ports to serve SSH over
# Default  :  []
# Type     :  list(string|integer)
ssh_config_port: ['22']
```

``` yaml
## Address family to serve SSH to
# Default  :  inet
# Type     :  string
# Options  :  any|inet|inet6
ssh_config_addressfamily: inet
```

``` yaml
## Address to listen for connections from.
# Default  :  ['0.0.0.0']
# Type     :  list(strings)
ssh_config_listenaddress: ['0.0.0.0']
```

``` yaml
## Routing domain
# Default  :  none
# Type     :  string
# Options  :  none|domain_name
ssh_config_rdomain: 'none'
```

#### Global Authentication

``` yaml
## Authentication Methods
## See documentation for details.
# Default  :  any
# Type     :  string
# Options  :  any|none|(publickey,password,hostbased,keyboard-interactive,...)
ssh_config_authenticationmethods: any
```

``` yaml
## Keyboard interactive authentication
# Default  :  false
# Type     :  bool
ssh_config_kbdinteractiveauthentication: false
```

``` yaml
## Permit root login
# Default  :  false
# Type     :  bool
ssh_config_permitrootlogin: false
```

``` yaml
## Use PAM
# Default  :  true
# Type     :  bool
ssh_config_usepam: true
```

#### Password Authentication

``` yaml
## Password authentication
# Default  :  true
# Type     :  bool
ssh_config_passwordauthentication: true
```

``` yaml
## Permit empty passwords
# Default  :  false
# Type     :  bool
ssh_config_permitemptypasswords: false
```

#### Public Key Authentication

``` yaml
## Public Key Authentication
# Default  :  true
# Type     :  bool
ssh_config_pubkeyauthentication: true
```

``` yaml
## Authorized Keys file
# Default  :  '.ssh/authorized_keys'
# Type     :  string
ssh_config_authorizedkeysfile: .ssh/authorized_keys
```

``` yaml
## Trusted user CA keys
# Default  :  'none'
# Type     :  none|filename
ssh_config_trustedusercakeys: 'none'
```

``` yaml
## Revoked keys file
# Default  :  'none'
# Type     :  none|filename
ssh_config_revokedkeys: 'none'
```

``` yaml
## Authorized Keys command
# Default  :  'none'
# Type     :  none|command
ssh_config_authorizedkeyscommand: 'none'
```

``` yaml
## Authorized Keys command user
# Default  :  'none'
# Type     :  none|username
ssh_config_authorizedkeyscommanduser: 'none'
```

``` yaml
## Authorized Principals command
# Default  :  'none'
# Type     :  none|command
ssh_config_authorizedprincipalscommand: 'none'
```

``` yaml
## Authorized Principals command user
# Default  :  'none'
# Type     :  none|username
ssh_config_authorizedprincipalscommanduser: 'none'
```

``` yaml
## Authorized Principals file
# Default  :  'none'
# Type     :  none|filename
ssh_config_authorizedprincipalsfile: 'none'
```

#### Kerberos Authentication

``` yaml
## Kerberos Authentication
# Default  :  false
# Type     :  bool
ssh_config_kerberosauthentication: false
```

``` yaml
## Kerberos or local password
# Default  :  false
# Type     :  bool
ssh_config_kerberosorlocalpasswd: false
```

``` yaml
## Kerberos get AFS token
# Default  :  false
# Type     :  bool
ssh_config_kerberosgetafstoken: false
```

``` yaml
## Kerberos Ticket Cleanup
# Default  :  false
# Type     :  bool
ssh_config_kerberosticketcleanup: false
```

#### GSSAPI Authentication

``` yaml
## GSSAPI Authentication
# Default  :  false
# Type     :  bool
ssh_config_gssapiauthentication: false
```

``` yaml
## GSSAPI cleanup credentials
# Default  :  false
# Type     :  bool
ssh_config_gssapicleanupcredentials: false
```

``` yaml
## GSSAPI strict accept or check
# Default  :  false
# Type     :  bool
ssh_config_gssapistrictacceptorcheck: false
```

#### Host Based Authentication

``` yaml
## Host Based Authentication
# Default  :  false
# Type     :  bool
ssh_config_hostbasedauthentication: false
```

``` yaml
## Host based uses name from packet only
# Default  :  false
# Type     :  bool
ssh_config_hostbasedusesnamefrompacketonly: false
```

``` yaml
## Ignore rhosts
# Default  :  true
# Type     :  bool
ssh_config_ignorerhosts: true
```

``` yaml
## Ignore user known hosts
# Default  :  true
# Type     :  bool
ssh_config_ignoreuserknownhosts: true
```

``` yaml
## Host Key agent
# Default  :  none
# Type     :  string
# Options  :  none|socket
ssh_config_hostkeyagent: 'none'
```

``` yaml
## Host Certificate
# Type     :  string
# Options  :  none|filename
ssh_config_hostcertificate: 'none'
```

#### Print

``` yaml
## Print last log
# Default  :  false
# Type     :  bool
ssh_config_printlastlog: false
```

``` yaml
## Print MOTD
# Default  :  false
# Type     :  bool
ssh_config_printmotd: false
```

``` yaml
## Banner
# Type     :  string
# Options  :  none|filename
ssh_config_banner: 'none'
```

``` yaml
## Version Addendum
# Type     :  string
# Options  :  none|string
ssh_config_versionaddendum: 'none'
```

#### Permissions

``` yaml
## Deny Users
## Space separated string.
# Type     :  string
# Options  :  none|string
ssh_config_denyusers: 'none'
```

``` yaml
## Allow Users
## Space separated string.
# Type     :  string
# Options  :  any|string
ssh_config_allowusers: 'any'
```

``` yaml
## Deny Groups
## Space separated string.
# Type     :  string
# Options  :  none|string
ssh_config_denygroups: 'none'
```

``` yaml
## Allow Groups
## Space separated string.
# Type     :  string
# Options  :  any|string
ssh_config_allowgroups: 'any'
```

#### Connection Settings

``` yaml
## Client alive count max
# Default  :  3
# Type     :  integer
ssh_config_clientalivecountmax: 3
```

``` yaml
## Client alive interval
# Default  :  0
# Type     :  integer
ssh_config_clientaliveinterval: 0
```

``` yaml
## Login grace time
# Default  :  120
# Type     :  integer
ssh_config_logingracetime: 120
```

``` yaml
## Max authentication tries
## Important if users have multiple keys.
# Default  :  6
# Type     :  integer
ssh_config_maxauthtries: 6
```

``` yaml
## Max sessions
# Default  :  10
# Type     :  integer
ssh_config_maxsessions: 10
```

``` yaml
## Max startups
# Default  :  '10:30:100'
# Type     :  string
ssh_config_maxstartups: '10:30:100'
```

``` yaml
## TCP Keepalive
# Default  :  false
# Type     :  bool
ssh_config_tcpkeepalive: false
```

``` yaml
## Rekey limit
# Default  :  'default none'
# Type     :  string
ssh_config_rekeylimit: default none
```

<!-- markdownlint-disable-next-line MD024 -->
#### Settings

``` yaml
## Log level
# Default  :  'INFO'
# Type     :  string
ssh_config_loglevel: INFO
```

``` yaml
## Syslog facility
# Default  :  'AUTH'
# Type     :  string
ssh_config_syslogfacility: AUTH
```

``` yaml
## Expose auth info
# Default  :  false
# Type     :  bool
ssh_config_exposeauthinfo: false
```

``` yaml
## Use DNS
# Default  :  false
# Type     :  bool
ssh_config_usedns: false
```

``` yaml
## Strict modes
# Default  :  true
# Type     :  bool
ssh_config_strictmodes: true
```

``` yaml
## Compression
# Default  :  true
# Type     :  bool
ssh_config_compression: true
```

#### Environment

``` yaml
## Accept environment
# Default  :  []
# Type     :  list(strings)
ssh_config_acceptenv: []
```

``` yaml
## Set environment
# Default  :  []
# Type     :  list(strings)
ssh_config_setenv: []
```

``` yaml
## Permit user environment
# Default  :  false
# Type     :  bool
ssh_config_permituserenvironment: false
```

``` yaml
## Permit user rc
# Default  :  true
# Type     :  bool
ssh_config_permituserrc: true
```

``` yaml
## Permit user rc
# Default  :  none
# Type     :  string
# Options  :  none|command
ssh_config_forcecommand: 'none'
```

#### Forwarding

``` yaml
## Disable forwarding
# Default  :  false
# Type     :  bool
ssh_config_disableforwarding: false
```

``` yaml
## Permit TTY
# Default  :  true
# Type     :  bool
ssh_config_permittty: true
```

``` yaml
## Allow agent forwarding
# Default  :  true
# Type     :  bool
ssh_config_allowagentforwarding: true
```

``` yaml
## Allow TCP forwarding
# Default  :  true
# Type     :  bool
ssh_config_allowtcpforwarding: true
```

``` yaml
## Permit tunnel
# Default  :  false
# Type     :  bool
ssh_config_permittunnel: false
```

``` yaml
## Permit listen
# Default  :  []
# Type     :  list(strings)
ssh_config_permitlisten: []
```

``` yaml
## Permit open
# Default  :  []
# Type     :  list(strings)
ssh_config_permitopen: []
```

``` yaml
## gateway ports
# Default  :  no
# Type     :  string
# Options  :  no|yes|clientspecified
ssh_config_gatewayports: 'no'
```

``` yaml
## Allow stream local forwarding
# Default  :  true
# Type     :  bool
ssh_config_allowstreamlocalforwarding: true
```

``` yaml
## Stream local bind mask
# Default  :  '0177'
# Type     :  string
ssh_config_streamlocalbindmask: '0177'
```

``` yaml
## Stream local bind unlink
# Default  :  false
# Type     :  bool
ssh_config_streamlocalbindunlink: false
```

``` yaml
## X11 forwarding
# Default  :  true
# Type     :  bool
ssh_config_x11forwarding: true
```

``` yaml
## X11 display offset
# Default  :  10
# Type     :  integer
ssh_config_x11displayoffset: 10
```

``` yaml
## X11 use localhost
# Default  :  true
# Type     :  bool
ssh_config_x11uselocalhost: true
```

``` yaml
## Xauth location
# Default  :  /usr/local/bin/xauth
# Type     :  string
ssh_config_xauthlocation: /usr/local/bin/xauth
```

#### System

``` yaml
## Chroot directory
# Type     :  string
# Options  :  none|directory
ssh_config_chrootdirectory: 'none'
```

``` yaml
## PID file
# Default  :  /var/run/sshd.pid
# Type     :  string
ssh_config_pidfile: /var/run/sshd.pid
```

``` yaml
## IPQoS
# Default  :  'af21 cs1'
# Type     :  string
ssh_config_ipqos: 'af21 cs1'
```

``` yaml
## Subsystem
# Default  :  ['sftp internal-sftp']
# Type     :  list(strings)
ssh_config_subsystem: ['sftp internal-sftp']
```

``` yaml
## Use blacklist
# Default  :  false
# Type     :  bool
ssh_config_useblacklist: false
```

#### Encryption

``` yaml
## Allowed host keys
# Default  :  ['ed25519', 'ecdsa', 'rsa']
# Type     :  list(strings)
ssh_config_allowed_hostkeys: ['ed25519', 'ecdsa', 'rsa']
```

``` yaml
## Ciphers
# Default  :  'default' (OpenSSH default)
# Type     :  string
ssh_config_ciphers: 'default'
```

``` yaml
## MACs
# Default  :  'default' (OpenSSH default)
# Type     :  string
ssh_config_macs: 'default'
```

``` yaml
## Key exchange algorithms
# Default  :  'default' (OpenSSH default)
# Type     :  string
ssh_config_kexalgorithms: 'default'
```

``` yaml
## Host key algorithms
# Default  :  'default' (OpenSSH default)
# Type     :  string
ssh_config_hostkeyalgorithms: 'default'
```

``` yaml
## Public key accepted key types
# Default  :  'default' (OpenSSH default)
# Type     :  string
ssh_config_pubkeyacceptedkeytypes: 'default'
```

``` yaml
## Host based accepted key types
# Default  :  'default' (OpenSSH default)
# Type     :  string
ssh_config_hostbasedacceptedkeytypes: 'default'
```

``` yaml
## Fingerprint Hash
# Default  :  'default' (OpenSSH default)
# Type     :  string
ssh_config_fingerprinthash: 'default'
```

``` yaml
## Managed host keys
## User defined hostkeys for host
## Should only be used if a host needs specific, predefined keys
# Default  :  []
# Type     :  list(dict)
ssh_config_managed_hostkeys: []

# Keys:
ssh_config_managed_hostkeys:
    ## Type of hostkey
    ## Will populate x in host_x_key in key name.
    # Required : true
    # Type     : string
  - type: 'ed25519'

    ## Private key content
    # Required : true
    # Type     : string
    private_key: 'contents'

    ## Public key content
    # Required : true
    # Type     : string
    public_key: 'contents'
```
