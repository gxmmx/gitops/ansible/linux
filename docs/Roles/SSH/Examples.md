<!-- markdownlint-disable MD041 -->
[wiki](home) /
[Roles](Roles) /
**[SSH](Roles/SSH)** /

## Simple

Simple example that configures SSH.
No password login and no root login.

**📄 `site.yml`**

``` yaml
- name: Simple SSH Play
  hosts: all

  vars:
    ssh_settings_validate_connection: false
    ssh_config_permitrootlogin: false
    ssh_config_passwordauthentication: false

  roles:
    - gxmmx.linux.ssh
```

## Hardened

Hardened example, that denies various forwarding.  
Only allows Public Key authentication and no root login.

**📄 `site.yml`**

``` yaml
- name: Hardened SSH Play
  hosts: all

  vars:
    ssh_settings_validate_connection: true

    ssh_config_authenticationmethods: publickey
    ssh_config_passwordauthentication: false
    ssh_config_permitrootlogin: false
    ssh_config_acceptenv: ['MY_ENV_VAR']
    ssh_config_x11forwarding: false
    ssh_config_allowtcpforwarding: false
    ssh_config_allowagentforwarding: false

  roles:
    - gxmmx.linux.ssh
```

## Full

Full example of SSH configuration.  
All config parameters added with default values.

**📄 `site.yml`**

``` yaml
- name: Full SSH Play
  hosts: all

  vars:
    ssh_settings_purge_hostkeys: false
    ssh_settings_validate_connection: true

    ssh_config_port: ['22']
    ssh_config_addressfamily: inet
    ssh_config_listenaddress: ['0.0.0.0']
    ssh_config_rdomain: 'none'
    ssh_config_authenticationmethods: any
    ssh_config_kbdinteractiveauthentication: false
    ssh_config_permitrootlogin: false
    ssh_config_usepam: true
    ssh_config_passwordauthentication: true
    ssh_config_permitemptypasswords: false
    ssh_config_pubkeyauthentication: true
    ssh_config_authorizedkeysfile: .ssh/authorized_keys
    ssh_config_trustedusercakeys: 'none'
    ssh_config_revokedkeys: 'none'
    ssh_config_authorizedkeyscommand: 'none'
    ssh_config_authorizedkeyscommanduser: 'none'
    ssh_config_authorizedprincipalscommand: 'none'
    ssh_config_authorizedprincipalscommanduser: 'none'
    ssh_config_authorizedprincipalsfile: 'none'
    ssh_config_kerberosauthentication: false
    ssh_config_kerberosorlocalpasswd: false
    ssh_config_kerberosgetafstoken: false
    ssh_config_kerberosticketcleanup: false
    ssh_config_gssapiauthentication: false
    ssh_config_gssapicleanupcredentials: false
    ssh_config_gssapistrictacceptorcheck: false
    ssh_config_hostbasedauthentication: false
    ssh_config_hostbasedusesnamefrompacketonly: false
    ssh_config_ignorerhosts: true
    ssh_config_ignoreuserknownhosts: true
    ssh_config_hostkeyagent: 'none'
    ssh_config_hostcertificate: 'none'
    ssh_config_printlastlog: false
    ssh_config_printmotd: false
    ssh_config_banner: 'none'
    ssh_config_versionaddendum: 'none'
    ssh_config_denyusers: 'none'
    ssh_config_allowusers: 'none'
    ssh_config_denygroups: 'none'
    ssh_config_allowgroups: 'none'
    ssh_config_clientalivecountmax: 3
    ssh_config_clientaliveinterval: 0
    ssh_config_logingracetime: 120
    ssh_config_maxauthtries: 6
    ssh_config_maxsessions: 10
    ssh_config_maxstartups: '10:30:100'
    ssh_config_tcpkeepalive: false
    ssh_config_rekeylimit: default none
    ssh_config_loglevel: INFO
    ssh_config_syslogfacility: AUTH
    ssh_config_exposeauthinfo: false
    ssh_config_usedns: true
    ssh_config_strictmodes: true
    ssh_config_compression: true
    ssh_config_acceptenv: []
    ssh_config_setenv: []
    ssh_config_permituserenvironment: false
    ssh_config_permituserrc: true
    ssh_config_forcecommand: 'none'
    ssh_config_disableforwarding: false
    ssh_config_permittty: true
    ssh_config_allowagentforwarding: true
    ssh_config_allowtcpforwarding: true
    ssh_config_permittunnel: false
    ssh_config_permitlisten: []
    ssh_config_permitopen: []
    ssh_config_gatewayports: 'no'
    ssh_config_allowstreamlocalforwarding: true
    ssh_config_streamlocalbindmask: 0177
    ssh_config_streamlocalbindunlink: false
    ssh_config_x11forwarding: true
    ssh_config_x11displayoffset: 10
    ssh_config_x11uselocalhost: true
    ssh_config_xauthlocation: /usr/local/bin/xauth
    ssh_config_chrootdirectory: 'none'
    ssh_config_pidfile: /var/run/sshd.pid
    ssh_config_ipqos: af21 cs1
    ssh_config_subsystem: ['sftp internal-sftp']
    ssh_config_useblacklist: false
    ssh_config_allowed_hostkeys: ['ed25519', 'ecdsa', 'rsa']
    ssh_config_managed_hostkeys: []
    ssh_config_ciphers: 'default'
    ssh_config_macs: 'default'
    ssh_config_kexalgorithms: 'default'
    ssh_config_hostkeyalgorithms: 'default'
    ssh_config_pubkeyacceptedkeytypes: 'default'
    ssh_config_hostbasedacceptedkeytypes: 'default'
    ssh_config_fingerprinthash: 'default'

  roles:
    - gxmmx.linux.ssh
```
