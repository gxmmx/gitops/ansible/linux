---
- name: Verify Converge
  hosts: all
  become: true

  vars:
    root_wrong_password_content: "mmuvQpdhCtJG2GI876NxYwUE"
    root_right_password_content: "WppbViTmyUzFik149xjdmK7k"
    root_right_keys_content: "IOdVCbMHtv8chAP690LP26ij"

    prov_id_content: "svc_provision:x:3000:3000"
    prov_wrong_comment_content: "provision_converge0_comment"

    user_with_all_id: 2001
    user_with_all_id_content: "user_with_all:x:2001:2001"
    user_with_all_password_content: "yhGKiJSLCQSEUs9htRfLZ2Qp"
    user_with_all_comment_content: "user_with_all_gecos"
    user_with_all_sudo_content: "/bin/true"
    user_with_all_key1_content: "user_with_all_key1"
    user_with_all_key2_content: "user_with_all_key2"
    user_with_all_expiry_content: "Jan 20, 2030"

    user_with_key_changes_right_key_content: "converge_key"
    user_with_key_changes_wrong_key_content: "manually_added_key"

    user_with_sudo_changes_right_sudo_content: "/bin/true"
    user_with_sudo_changes_wrong_sudo_content: "ALL=(ALL:ALL) ALL"

    user_to_be_disabled_expiry_content: "1970"

    group_with_sudo_sudo_content: "/bin/true"

  tasks:
    # Init
    - name: Init - Initialize role for testing
      ansible.builtin.include_role:
        name: gxmmx.linux.auth
        tasks_from: init

    # Prepare lists
    - name: Prepare - list - passwd
      ansible.builtin.command: cat /etc/passwd
      changed_when: false
      register: passwd_list

    - name: Prepare - list - shadow
      ansible.builtin.command: cat /etc/shadow
      changed_when: false
      register: shadow_list

    - name: Prepare - list - group
      ansible.builtin.command: cat /etc/group
      changed_when: false
      register: group_list

    # Prepare root
    - name: Prepare - stat - root pubkeys
      ansible.builtin.stat:
        path: "/root/{{ auth_vars_ssh_folder }}/authorized_keys"
      register: root_keys_file

    - name: Prepare - content - root pubkeys
      ansible.builtin.command: "cat /root/{{ auth_vars_ssh_folder }}/authorized_keys"
      changed_when: false
      register: root_keys

    # Prepare user_to_be_disabled
    - name: Prepare - details - user_to_be_disabled expiry
      ansible.builtin.command: chage -l user_to_be_disabled
      changed_when: false
      register: user_to_be_disabled_chage

    # Prepare user_with_all
    - name: Prepare - stat - user_with_all sudo
      ansible.builtin.stat:
        path: "{{ auth_vars_sudoers_dir }}/user_with_all"
      register: user_with_all_sudo_file

    - name: Prepare - content - user_with_all sudo
      ansible.builtin.command: "cat {{ auth_vars_sudoers_dir }}/user_with_all"
      changed_when: false
      register: user_with_all_sudo

    - name: Prepare - stat - user_with_all pubkeys
      ansible.builtin.stat:
        path: "/user_with_all_home/{{ auth_vars_ssh_folder }}/authorized_keys"
      register: user_with_all_keys_file

    - name: Prepare - content - user_with_all pubkeys
      ansible.builtin.command: "cat /user_with_all_home/{{ auth_vars_ssh_folder }}/authorized_keys"
      changed_when: false
      register: user_with_all_keys

    - name: Prepare - details - user_with_all expiry
      ansible.builtin.command: chage -l user_with_all
      changed_when: false
      register: user_with_all_chage

     # Prepare user_with_key_changes
    - name: Prepare - content - user_with_key_changes pubkeys
      ansible.builtin.command: "cat /home/user_with_key_changes/{{ auth_vars_ssh_folder }}/authorized_keys"
      changed_when: false
      register: user_with_key_changes_keys

    # Prepare user_with_sudo_changes
    - name: Prepare - content - user_with_sudo_changes sudo
      ansible.builtin.command: "cat {{ auth_vars_sudoers_dir }}/user_with_sudo_changes"
      changed_when: false
      register: user_with_sudo_changes_sudo

    # Prepare user_with_no_sudo
    - name: Prepare - stat - user_with_no_sudo sudo
      ansible.builtin.stat:
        path: "{{ auth_vars_sudoers_dir }}/user_with_no_sudo"
      register: user_with_no_sudo_file

    # Prepare group_with_sudo
    - name: Prepare - stat - group_with_sudo sudo
      ansible.builtin.stat:
        path: "{{ auth_vars_sudoers_dir }}/group_with_sudo"
      register: group_with_sudo_sudo_file

    - name: Prepare - content - group_with_sudo sudo
      ansible.builtin.command: "cat {{ auth_vars_sudoers_dir }}/group_with_sudo"
      changed_when: false
      register: group_with_sudo_sudo

    # Test root
    - name: Test - root
      ansible.builtin.assert:
        that:
          - "root_right_password_content in shadow_list.stdout"
          - "root_wrong_password_content not in shadow_list.stdout"

          - "root_keys_file.stat.exists"
          - "root_keys_file.stat.uid == 0"
          - "root_keys_file.stat.mode == '0600'"
          - "root_right_keys_content in root_keys.stdout"
        success_msg: User root ok

    # Test provision
    - name: Test - provision
      ansible.builtin.assert:
        that:
          - "prov_id_content in passwd_list.stdout"
          - "prov_wrong_comment_content not in passwd_list.stdout"
        success_msg: User provision ok

    # Test user_to_be_removed
    - name: Test - user_to_be_removed
      ansible.builtin.assert:
        that:
          - "'user_to_be_removed' not in passwd_list.stdout"
        success_msg: User user_to_be_removed ok

    # Test user_to_be_disabled
    - name: Test - user_to_be_disabled
      ansible.builtin.assert:
        that:
          - "user_to_be_disabled_expiry_content in user_to_be_disabled_chage.stdout"
        success_msg: User user_to_be_disabled ok

    # Test user_with_all
    - name: Test - user_with_all
      ansible.builtin.assert:
        that:
          - "user_with_all_id_content in passwd_list.stdout"
          - "'zsh' in passwd_list.stdout"
          - "user_with_all_comment_content in passwd_list.stdout"
          - "user_with_all_password_content in shadow_list.stdout"
          - "user_with_all_expiry_content in user_with_all_chage.stdout"

          - "user_with_all_sudo_file.stat.exists"
          - "user_with_all_sudo_file.stat.uid == 0"
          - "user_with_all_sudo_file.stat.gid == 0"
          - "user_with_all_sudo_file.stat.mode == '0440'"
          - "user_with_all_sudo_content in user_with_all_sudo.stdout"

          - "user_with_all_keys_file.stat.exists"
          - "user_with_all_keys_file.stat.uid == user_with_all_id"
          - "user_with_all_keys_file.stat.mode == '0600'"
          - "user_with_all_key1_content in user_with_all_keys.stdout"
          - "user_with_all_key2_content in user_with_all_keys.stdout"
        success_msg: User user_with_all ok

    # Test user_with_key_changes
    - name: Test - user_with_key_changes
      ansible.builtin.assert:
        that:
          - "user_with_key_changes_right_key_content in user_with_key_changes_keys.stdout"
          - "user_with_key_changes_wrong_key_content not in user_with_key_changes_keys.stdout"
        success_msg: User user_with_key_changes ok

    # Test user_with_sudo_changes
    - name: Test - user_with_sudo_changes
      ansible.builtin.assert:
        that:
          - "user_with_sudo_changes_right_sudo_content in user_with_sudo_changes_sudo.stdout"
          - "user_with_sudo_changes_wrong_sudo_content not in user_with_sudo_changes_sudo.stdout"
        success_msg: User user_with_sudo_changes ok

    # Test user_with_no_sudo
    - name: Test - user_with_no_sudo
      ansible.builtin.assert:
        that:
          - "not user_with_no_sudo_file.stat.exists"
        success_msg: User user_with_no_sudo ok

    # Test group_with_sudo
    - name: Test - group_with_sudo
      ansible.builtin.assert:
        that:
          - "'group_with_sudo' in group_list.stdout"
          - "group_with_sudo_sudo_file.stat.exists"
          - "group_with_sudo_sudo_file.stat.uid == 0"
          - "group_with_sudo_sudo_file.stat.gid == 0"
          - "group_with_sudo_sudo_file.stat.mode == '0440'"
          - "group_with_sudo_sudo_content in group_with_sudo_sudo.stdout"
        success_msg: Group group_with_sudo ok
