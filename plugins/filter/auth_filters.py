#!/usr/bin/python
# Filter plugins for data manipulation

from datetime import datetime
from ansible.errors import AnsibleError

class FilterModule(object):
    def filters(self):
        return {
            '_auth_parse_lists': self.auth_parse_lists
        }

    # Parses and validates user list
    def auth_parse_lists(self, user_list, group_list, parsing_vars):
        names = set()
        ids = set()
        auth_list = []

        # ----------------------------------------------------------------------
        # Root user
        if parsing_vars['manage_root']:
            root_password = parsing_vars['root_password']
            if not root_password.strip():
                raise AnsibleError('Root password can not be empty')

            root_user = {
                'type': 'user',
                'name': 'root',
                'group': 'root',
                'expires': float(-1),
                'state': 'present',
                'id': '',
                'comment': '',
                'shell': '',
                'home': '',
                'home_create': True,
                'groups': '',
                'password': root_password,
                'package': '',
                'sudo': [],
                'pubkeys': []
            }

            # Keys
            root_keys = parsing_vars['root_pubkeys']
            if root_keys:
                if isinstance(root_keys, str):
                    root_keys = [root_keys]
                root_user['pubkeys'] = root_keys

            # Add root to list
            auth_list.append(root_user)

        # ----------------------------------------------------------------------
        # Stop parsing if list should not be managed
        if not parsing_vars['manage_list']:
            return auth_list

        # ----------------------------------------------------------------------
        # User list
        for user_item in user_list:
            user = {
                'type': 'user',
                'name': '',
                'group': '',
                'expires': float(-1),
                'state': 'present',
                'id': '',
                'comment': '',
                'shell': '',
                'home': '',
                'home_create': True,
                'groups': '',
                'password': '!',
                'package': '',
                'sudo': [],
                'pubkeys': []
            }

            # Name & Primary group
            if 'name' not in user_item:
                raise AnsibleError('User field missing: name')
            if not user_item['name']:
                raise AnsibleError('User field empty: name')
            if user_item['name'] == parsing_vars['provision_user']:
                if not parsing_vars['manage_provision']:
                    continue
            if user_item['name'] == 'root':
                raise AnsibleError('User root can not be defined in list. See documentation for managing root user.')
            if user_item['name'] in parsing_vars['admin_groups']:
                raise AnsibleError('User name can not be one of admin groups.')
            if user_item['name'] in names:
                raise AnsibleError('User field must be unique: name')

            names.add(user_item['name'])
            user['name'] = user_item['name']
            user['group'] = user_item['name']

            # Expiry
            if 'expires' in user_item and user_item['expires']:
                if user_item['expires'] == 'never':
                    user['expires'] = float(-1)
                else:
                    dt = user_item['expires']
                    try:
                        date_obj = datetime.strptime(dt, "%Y-%m-%d")
                    except:
                        raise AnsibleError("Invalid date format. Please use the format yyyy-mm-dd.")
                    user['expires'] = date_obj.timestamp()

            # State
            if 'state' in user_item:
                if user_item['state'] not in ['present','absent','disabled']:
                    raise AnsibleError('User state can only be present, absent or disabled')
                
                if user_item['state'] == 'disabled':
                    if user_item['name'] == parsing_vars['provision_user']:
                        raise AnsibleError("Provision user can not be disabled.")
                    user['state'] = 'present'
                    user['expires'] = float(1)
                else:
                    if user_item['state'] == 'absent':
                        if user_item['name'] == parsing_vars['provision_user']:
                            raise AnsibleError("Provision user can not be removed.")
                    user['state'] = user_item['state']

            # ID
            if 'id' in user_item:
                if user_item['id'] in ids:
                    raise AnsibleError('User field must be unique: id')
                
                if 100 <= user_item['id'] <= 60000:
                    ids.add(user_item['id'])
                    user['id'] = user_item['id']
                else:
                    raise AnsibleError('User id must be an integer between 100-60000')

            # Comment
            if 'comment' in user_item and user_item['comment']:
                user['comment'] = user_item['comment']

            # Shell & package
            if 'shell' in user_item and user_item['shell']:
                shell = user_item['shell']

                if user_item['shell'] in parsing_vars['shells_native']:
                    user['shell'] = parsing_vars['shells_native'][shell]
                elif user_item['shell'] in parsing_vars['shells_installable']:
                    user['shell'] =  parsing_vars['shells_installable'][shell][0]
                    user['package'] = parsing_vars['shells_installable'][shell][1]
                else:
                    raise AnsibleError('User shell value unsupported. See documentation for supported shell values.')
            else:
                user['shell'] = parsing_vars['shells_native']['default']

            # Home
            if 'home' in user_item and user_item['home']:
                if user_item['home'] == 'none':
                    user['home_create'] = False
                elif user_item['home'] == 'default':
                    user['home_create'] = True
                else:
                    user['home'] = user_item['home']
                    user['home_create'] = True

            # Groups
            if 'groups' in user_item and user_item['groups']:
                if isinstance(user_item['groups'], str):
                    raise AnsibleError('Users groups must be a list.')
                groups = []
                for group in user_item['groups']:
                    if group in parsing_vars['admin_groups']:
                        groups.append(parsing_vars['admin_group'])
                    else:
                        groups.append(group)

                groups = list(set(groups))
                user['groups'] = ",".join(groups)

             # Password
            if 'password' in user_item and user_item['password']:
                if not user_item['password'] == '!':
                    hash_prefix_match = [prefix for prefix in parsing_vars['hash_prefixes'] if user_item['password'].startswith(prefix)]
                    if not hash_prefix_match:
                        raise AnsibleError('Users password does not seem to be a valid hash type.')
                    user['password'] = user_item['password']

            #  Sudo
            if 'sudo' in user_item and user_item['sudo']:
                if isinstance(user_item['sudo'], str):
                    raise AnsibleError('Users sudo definition must be a list.')
                user['sudo'] = user_item['sudo']

            # SSH public keys
            if 'pubkeys' in user_item:
                if not user['home_create']:
                    raise AnsibleError('Users home must be created for key management.')
                if isinstance(user_item['pubkeys'], str):
                    raise AnsibleError('Users pubkey definition must be a list.')
                user['pubkeys'] = user_item['pubkeys']
            
            # Add user to list
            auth_list.append(user)

        # ----------------------------------------------------------------------
        # Group list
        for group_item in group_list:
            group = {
                'type': 'group',
                'name': '',
                'state': 'present',
                'id': '',
                'sudo': [],
            }

            # Name
            if 'name' not in group_item:
                raise AnsibleError('Group field missing: name')
            if not group_item['name']:
                raise AnsibleError('Group field empty: name')
            if group_item['name'] == parsing_vars['provision_user']:
                if not parsing_vars['manage_provision']:
                    continue
            if group_item['name'] == 'root':
                raise AnsibleError('Group root can not be defined in list.')
            if group_item['name'] in parsing_vars['admin_groups']:
                raise AnsibleError('Group name can not be one of admin groups.')
            if group_item['name'] in names:
                raise AnsibleError('Group field must be unique: name')

            names.add(group_item['name'])
            group['name'] = group_item['name']

            # State
            if 'state' in group_item:
                if group_item['state'] not in ['present','absent']:
                    raise AnsibleError('Group state can only be present or absent')
                group['state'] = group_item['state']

            # ID
            if 'id' in group_item:
                if group_item['id'] in ids:
                    raise AnsibleError('Group field must be unique: id')
                
                if 100 <= group_item['id'] <= 60000:
                    ids.add(group_item['id'])
                    group['id'] = group_item['id']
                else:
                    raise AnsibleError('Group id must be an integer between 100-60000')

            #  Sudo
            if 'sudo' in group_item and group_item['sudo']:
                if isinstance(group_item['sudo'], str):
                    raise AnsibleError('Groups sudo definition must be a list.')
                group['sudo'] = group_item['sudo']

            # Add group to list
            auth_list.append(group)

        return auth_list
