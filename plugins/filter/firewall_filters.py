#!/usr/bin/python
# Filter plugins for data manipulation

from ansible.errors import AnsibleError
import ipaddress

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Returns version and valid ip list or value error
def valid_ip_list(ip_list):
    version = 0
    # Validate
    for ip_entry in ip_list:
        # Split to check for range
        ip_sublist = ip_entry.split("-")
        if len(ip_sublist) > 2:
            raise ValueError(f"Invalid IP range: {ip_entry}")
        elif len(ip_sublist) > 1:
            try:
                addr = ipaddress.ip_address(ip_sublist[0])
                addr2 = ipaddress.ip_address(ip_sublist[1])
                if addr.version != addr2.version:
                    raise ValueError(f"Invalid IP range: {ip_entry}")
            except:
                raise ValueError(f"Invalid IP in range: {ip_entry}")
        elif len(ip_sublist) == 1:
            try:
                addr = ipaddress.ip_address(ip_sublist[0])
            except:
                try:
                    addr = ipaddress.ip_network(ip_sublist[0], strict=True)
                except:
                    raise ValueError(f"Invalid IP address: {ip_entry}")
        else:
            raise ValueError("IP address can not be empty")

        if version == 0:
            version = addr.version
        if addr.version != version:
            raise ValueError(f"Multiple IP versions in same list")

    # Return version
    return version

# Wrap multi element list with braces and return single element list
def list_to_nftables_braces(passed_list):
    if len(passed_list) > 1:
        element = '{ ' + ', '.join(passed_list) + ' }'
        return [element]
    return passed_list

# ------------------------------------------------------------------------------
# Filters
# ------------------------------------------------------------------------------

class FilterModule(object):
    def filters(self):
        return {
            '_firewall_validate_allows': self.firewall_validate_allows,
            '_firewall_nftables_inbound': self.firewall_nftables_inbound,
            '_firewall_nftables_outbound': self.firewall_nftables_outbound
        }

    def firewall_validate_allows(self, rule_list):
        rules = []
        for rule_dict in rule_list:
            rule = {
                'name': '',
                'comment': '',
                'policy': 'drop',
                'ipversion': 0,
                'interfaces': [],
                'sources': [],
                'destinations': [],
                'ports': [],
                'protocols': [],
            }

            # Name
            if 'name' not in rule_dict:
                raise AnsibleError('Rule field missing: name')
            if not rule_dict['name']:
                raise AnsibleError('Rule field empty: name')
            rule['name'] = rule_dict['name']

            # Comment
            if 'comment' in rule_dict and rule_dict['comment']:
                rule['comment'] = rule_dict['comment']
            else:
                rule['comment'] = rule_dict['name']

            # Policy
            if 'policy' in rule_dict and rule_dict['policy']:
                if rule_dict['policy'] not in ['drop', 'accept']:
                    raise AnsibleError(f"{rule['name']} - Invalid rule policy. Must be accept or drop")
                rule['policy'] = rule_dict['policy']
            else: 
                rule['policy'] = 'drop'

            # Interfaces
            if 'interface' in rule_dict and rule_dict['interface']:
                rule['interfaces'] = rule_dict['interface'] if isinstance(rule_dict['interface'], list) else [rule_dict['interface']]

            # Sources & IP version
            if 'source' in rule_dict and rule_dict['source']:
                rule['sources'] = rule_dict['source'] if isinstance(rule_dict['source'], list) else [rule_dict['source']]
                try:
                    ip_version = valid_ip_list(rule['sources'])
                except ValueError as e:
                    raise AnsibleError(f"{rule['name']} - Invalid source. {e}")
                if rule['ipversion'] == 0:
                    rule['ipversion'] = ip_version
            
            # Destinations & IP version
            if 'destination' in rule_dict and rule_dict['destination']:
                rule['destinations'] = rule_dict['destination'] if isinstance(rule_dict['destination'], list) else [rule_dict['destination']]
                try:
                    ip_version = valid_ip_list(rule['destinations'])
                except ValueError as e:
                    raise AnsibleError(f"{rule['name']} - Invalid desination. {e}")
                if rule['ipversion'] == 0:
                    rule['ipversion'] = ip_version
                if rule['ipversion'] != ip_version:
                    raise AnsibleError(f"{rule['name']} - Invalid rule. Source and destination IP versions differ")

            # Ports
            if 'port' in rule_dict and rule_dict['port']:
                rule['ports'] = rule_dict['port'] if isinstance(rule_dict['port'], list) else [rule_dict['port']]
                rule['ports'] = [str(port) for port in rule['ports']]

                # Protocols
                if 'protocol' in rule_dict and rule_dict['protocol']:
                    if rule_dict['protocol'] not in ['any', 'tcp', 'udp']:
                        raise AnsibleError(f"{rule['name']} - Invalid rule protocol. Must be any, tcp or udp")
                    if rule_dict['protocol'] == 'any':
                        rule['protocols'] = ['tcp', 'udp']
                    if rule_dict['protocol'] == 'tcp':
                        rule['protocols'] = ['tcp']
                    if rule_dict['protocol'] == 'udp':
                        rule['protocols'] = ['udp']
                else:
                    rule['protocols'] = ['tcp', 'udp']

            # Validate that at least one was passed
            one_of = ['sources', 'destinations', 'interfaces', 'ports']
            if all(len(rule[key]) == 0 for key in one_of):
                raise AnsibleError(f"{rule['name']} - Invalid rule definition. Rule parameters missing")

            # Append to list
            rules.append(rule)

        # Return list
        return rules

    def firewall_nftables_inbound(self, rule_list):
        rules = []
        for rule_dict in rule_list:
            rule_obj = {
                'name': rule_dict['name'],
                'comment': rule_dict['comment'],
                'lines': []
            }
            rule_line = []
            if rule_dict['sources']:
                rule_line = rule_line + ['ip', 'saddr'] if rule_dict['ipversion'] == 4 else rule_line + ['ip6', 'saddr']
                rule_line = rule_line + list_to_nftables_braces(rule_dict['sources'])
            
            if rule_dict['destinations']:
                rule_line = rule_line + ['ip', 'daddr'] if rule_dict['ipversion'] == 4 else rule_line + ['ip6', 'daddr']
                rule_line = rule_line + list_to_nftables_braces(rule_dict['destinations'])
            
            if rule_dict['interfaces']:
                rule_line = rule_line + ['iifname'] + list_to_nftables_braces(rule_dict['interfaces'])
            
            policy = rule_dict['policy']
            comment = rule_dict['comment']
            rule_line = rule_line + [policy] + [f'comment "{comment}"']

            if not rule_dict['protocols']:
                line = ' '.join(rule_line)
                rule_obj['lines'].append(line)
            else:
                for proto in rule_dict['protocols']:
                    line = ' '.join([proto, 'dport'] + list_to_nftables_braces(rule_dict['ports']) + rule_line)
                    rule_obj['lines'].append(line)
            rules.append(rule_obj)
        return rules


    def firewall_nftables_outbound(self, rule_list):
        rules = []
        for rule_dict in rule_list:
            rule_obj = {
                'name': rule_dict['name'],
                'comment': rule_dict['comment'],
                'lines': []
            }
            rule_line = []
            if rule_dict['sources']:
                rule_line = rule_line + ['ip', 'saddr'] if rule_dict['ipversion'] == 4 else rule_line + ['ip6', 'saddr']
                rule_line = rule_line + list_to_nftables_braces(rule_dict['sources'])
            
            if rule_dict['destinations']:
                rule_line = rule_line + ['ip', 'daddr'] if rule_dict['ipversion'] == 4 else rule_line + ['ip6', 'daddr']
                rule_line = rule_line + list_to_nftables_braces(rule_dict['destinations'])
            
            if rule_dict['interfaces']:
                rule_line = rule_line + ['oifname'] + list_to_nftables_braces(rule_dict['interfaces'])
            
            policy = rule_dict['policy']
            comment = rule_dict['comment']
            rule_line = rule_line + [policy] + [f'comment "{comment}"']

            if not rule_dict['protocols']:
                line = ' '.join(rule_line)
                rule_obj['lines'].append(line)
            else:
                for proto in rule_dict['protocols']:
                    line = ' '.join([proto, 'sport'] + list_to_nftables_braces(rule_dict['ports']) + rule_line)
                    rule_obj['lines'].append(line)
            rules.append(rule_obj)
        return rules
