#!/usr/bin/env python3

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: gxmmx.linux.backup_file

short_description: Manages backup files

version_added: "1.0.0"

description: Manages backup files created by Ansibles template and file modules. Restore latest or specifc backup. Ensures rotation/removal of oldest backups.

options:
    path:
        description: Path of backed up file.
        required: true
        type: str
    restore:
        description: Restores either a specific backup or 'latest'
        required: false
        type: str
        default: none
    keep:
        description: Backups to keep, Latest will be retained.
        required: false
        type: int

author:
    - Gudmundur Gudmundsson (@gummigudm)
'''

EXAMPLES = r'''
- name: Ensure only 5 latest backups are retaind
  gxmmx.linux.backup_file:
    path: /path/to/file.conf
    keep: 5

- name: Restore latest backup
  gxmmx.linux.backup_file:
    path: /path/to/file.conf
    restore: latest
  when: some_issue_bool

- name: Restore specific backup
  gxmmx.linux.backup_file:
    path: /path/to/file.conf
    restore: template_registered.backup_file
  when: some_issue_bool

- name: restore specific backup when issue, but always run for retention
  gxmmx.linux.backup_file:
    path: /path/to/file.conf
    restore: "{{ template_registered.backup_file if some_issue else 'none' }}"
    keep: 5
'''

RETURN = r'''
cleanup:
    description: Indicates if cleanup was performed.
    type: bool
    returned: always
    sample: true
restored:
    description: Indicates if restore was performed.
    type: bool
    returned: always
    sample: true
restored_file:
    description: Path of file restored
    type: str
    returned: when restored
    sample: '/path/to/file.conf.1234.2024-01-18@12:23:45~'
'''

from ansible.module_utils.basic import AnsibleModule
import os
import re
import shutil

def fetch_backups(file_path):
    ansible_backup_regex = r'\d{2}-\d{2}-\d{2}@\d{2}:\d{2}:\d{2}~$'
    dir = os.path.dirname(file_path)
    base_file = os.path.basename(file_path)
    backups = []

    files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
    backup_files = [file for file in files if str(file).startswith(base_file) and re.search(ansible_backup_regex, file)]

    backup_tuples = []
    for file in backup_files:
        dtstr = str(file).rsplit('.', 1)[-1]
        dt = dtstr.translate({ord(i): None for i in '-~:@'})
        item = (os.path.join(dir, file), int(dt))
        backup_tuples.append(item)

    sorted_backup_tuples = sorted(backup_tuples, key=lambda x: x[1], reverse=True)
    backups = [os.path.join(dir, file[0]) for file in sorted_backup_tuples]
    return backups

def run_module():
    module_args = dict(
        path=dict(type='str', required=True),
        keep=dict(type='int', required=False),
        restore=dict(type='str', required=False, default="none")
    )

    result = dict(
        changed = False,
        cleanup = False,
        restored = False,
        restored_file = ''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    path = os.path.expanduser(module.params['path'])
    if not os.path.isfile(path):
        module.fail_json(msg='File does not exist: ' + path, **result)
    backups = fetch_backups(path)

    if module.params['restore'] == 'latest':
        if not module.check_mode:
            try:
                shutil.copyfile(backups[0], path)
                os.remove(backups[0])
            except:
                module.fail_json(msg='Could not restore backup: ' + backups[0], **result)
        result['changed'] = True
        result['restored'] = True
        result['restored_file'] = backups[0]

    elif module.params['restore'] in backups:
        if not module.check_mode:
            try:
                shutil.copyfile(module.params['restore'], path)
                os.remove(module.params['restore'])
            except:
                module.fail_json(msg='Could not restore backup: ' + module.params['restore'], **result)
        result['changed'] = True
        result['restored'] = True
        result['restored_file'] = module.params['restore']

    elif module.params['restore'] == 'none':
        pass

    else:
        module.fail_json(msg='Backup file does not exist: ' + module.params['restore'], **result)

    if module.params['keep']:
        if module.params['restore'] != 'none':
            backups = fetch_backups(path)

        backups_to_remove = []
        num_to_keep = int(module.params['keep'])
        if len(backups) > num_to_keep:
            backups_to_remove = backups[num_to_keep:]
            if not module.check_mode:
                for file in backups_to_remove:
                    try:
                        os.remove(file)
                    except:
                        module.fail_json(msg='Could not remove backup: ' + file, **result)
            result['changed'] = True
            result['cleanup'] = True

    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
