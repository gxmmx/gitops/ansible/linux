---
- name: Rollback - Ensure rollback script
  ansible.builtin.template:
    src: rollback.sh.j2
    dest: "{{ firewall_vars_nftables_rollback_file.path }}"
    owner: "{{ firewall_vars_nftables_rollback_file.owner }}"
    group: "{{ firewall_vars_nftables_rollback_file.group }}"
    mode: "{{ firewall_vars_nftables_rollback_file.mode }}"
    trim_blocks: yes
    lstrip_blocks: yes
    backup: no
  register: nftables_rollback_script_file_state
  when: firewall_settings_rollback_on_connection_failure | bool

- name: Rollback - Ensure service
  ansible.builtin.template:
    src: nftables-rollback.service.j2
    dest: "/etc/systemd/system/{{ firewall_vars_rollback_service }}"
    owner: root
    group: root
    mode: 0644
    trim_blocks: yes
    lstrip_blocks: yes
    backup: no
  register: nftables_rollback_systemd_file_state
  when: firewall_settings_rollback_on_connection_failure | bool

- name: State - Reload service
  ansible.builtin.systemd:
    daemon_reload: true
  when: 
    - firewall_settings_rollback_on_connection_failure | bool
    - nftables_rollback_systemd_file_state.changed

- name: State - Start rollback service
  ansible.builtin.systemd:
    name: "{{ firewall_vars_rollback_service }}"
    state: started
  changed_when: false
  when: firewall_settings_rollback_on_connection_failure | bool

- name: State - Wait for controller connection
  ansible.builtin.wait_for:
    port: "{{ ansible_port | default(22,true) }}"
    delay: 15
    timeout: "{{ firewall_settings_rollback_timeout + 60 }}"
    host: "{{ ansible_host }}"
  delegate_to: 127.0.0.1
  become: false
  when: firewall_settings_rollback_on_connection_failure | bool

- name: State - Check rollback state
  ansible.builtin.service_facts:
  register: firewall_rollback_service_state

- name: State - Ensure rollback state
  ansible.builtin.fail:
    msg: "Firewall rollback was performed due to connectivity issue from controller."
  when:
    - firewall_settings_rollback_on_connection_failure | bool
    - firewall_rollback_service_state['ansible_facts']['services'][firewall_vars_rollback_service]['state'] == 'inactive'

- name: State - Approve current state
  ansible.builtin.systemd:
    name: "{{ firewall_vars_rollback_service }}"
    state: stopped
  register: firewall_rollback_status
  changed_when: false
  when: firewall_settings_rollback_on_connection_failure | bool

- name: Rollback - Cleanup backups
  gummigudm.linux.backup_file:
    path: "{{ firewall_vars_nftables_config_main_file.path }}"
    keep: "{{ firewall_settings_rollback_backups }}"
  when: firewall_settings_rollback_on_connection_failure | bool
